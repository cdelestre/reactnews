# React News

**Get last news using NewsAPI and React**

## Install and configure the project

1. You need to register to https://newsapi.org/ in order to have an API Key.

2. Then copy .env.example file to .env file and fill variables :

```bash
cp .env.example .env
# Open .env with your favorite text editor or IDE
```

| Variable | Definition | Default Value |
| ------ | ------ | ------ |
| REACT_APP_NEWS_API_KEY | NewsAPI Key | **Mandatory and no default value are given !**|
| REACT_APP_REFRESH_INTERVAL | Number of minute the app will refresh and fetch 4 last news|15|

## Launch the project

```bash
yarn start
```

## TODO List

- [x] Fetch 4 last news (**High priorirty**)
- [x] Disply news (**High priorirty**)
- [x] Disply news image (**High priorirty**)
- [x] Allow user to click on each news to see the full description (**High priorirty**)
- [x] Handle empty values : for author or description for example (**High priorirty**)
- [x] Handle empty urlToImage value + maximum height maybe ? (**Normal priorirty**)
- [x] Handle date formating (**High priorirty**)
- [x] Refresh news (**High priorirty**)
- [x] Allow user to choose among several languages (**Normal priorirty**)
- [x] Improve APP/Layout and CSS... (**Normal priorirty**)
- [x] Hide API token (**High priorirty**)
