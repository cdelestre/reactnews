import React, { useState, useEffect } from "react";
import News from "./News";

function NewsList() {
  const [newsList, setnewsList] = useState([]);
  const [language, setLanguage] = useState('fr');
  const languagesChoices = {
                            'Australia':'au',
                            'Belgium':'be',
                            'Canada':'ca',
                            'Dutchland':'de',
                            'France':'fr',
                            'Grand Britain':'gb',
                            'Italy':'it',
                            'Japan':'jp',
                            'South Korea':'kr',
                            'New Zealand':'nz',
                            'USA':'us'
                          };
  const pageSize = '4';
  const API_KEY = process.env.REACT_APP_NEWS_API_KEY;
  const REFRESH_INTERVAL = parseInt(process.env.REACT_APP_REFRESH_INTERVAL);
  let interval;


  async function getLastNews() {
    try{
      const response = await fetch(`http://newsapi.org/v2/top-headlines?country=${language}&apiKey=${API_KEY}&pageSize=${pageSize}`, {
        method: "GET",
      });
      if (response.ok){
        const json = await response.json();
        setnewsList(s => s = json.articles);
      }
    }
    catch(error){
      console.log('Error while fetching data : ',error);
    }
  }

  useEffect( () => {
    clearInterval(interval);
    getLastNews();
    interval=setInterval(()=>{
      getLastNews()
    }, 60000*REFRESH_INTERVAL)
     return()=>clearInterval(interval)
  }, [language]);

  function handleLanguageChange(e, language){
      let value = e.target.value;
      setLanguage(value);
  }

  return (
      <div className="main-wrapper">
        <div className="body-wrapper">
          <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-auto">
              <label className="form-label" for="language">
                Please choose a country :
              </label>
            </div>
            <div className="col-md-auto">
              <select id="language" name="language"
                      className="form-select"
                      onChange={handleLanguageChange}
                      defaultValue={language}>
                {
                  Object.keys(languagesChoices).map((key, index) => {
                      return(
                        <option key={index} value={languagesChoices[key]}>
                          {key}
                        </option>
                      )
                  })
                }
              </select>
            </div>
          </div>
          <br />
          <strong className="ms-5">
            News will be refresh every {REFRESH_INTERVAL} minutes from last language change.
          </strong>
          <div className="row">
            {newsList.length > 0 && newsList.map((item, index) => (
              <News key={index} news={item} id={index}>
              </News>
            ))}
          </div>
          <hr />
          <p className="text-center fst-italic">
            Made using
            <a href="https://reactjs.org/"> React </a>
            and
            <a href="https://newsapi.org/"> News API </a>
            by
            <a href="https://gitlab.com/cdelestre"> cdelestre </a>
            2020
          </p>
        </div>
      </div>
    </div>
  );
}

export default NewsList;
