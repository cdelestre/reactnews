import React from 'react';

function News({id, news} ){

  function getDateStringWithSpaces(stringDate){
    return stringDate.replace('T',' at ').replace('Z', ' ');
  }

  return(
    <div className="col-md-5 offset-md-1 rounded-3
                    bg-light align-self-center mt-3">
      <strong>{news.title}</strong>
      <br />
      <img className="img-fluid rounded news-picture"
           src={news.urlToImage}
           alt="No picture available" />
      <br />
      <div className="text-center">
        <button type="button"
                className="btn btn-primary my-2"
                data-bs-toggle="modal"
                data-bs-target={"#news"+id}>
          More details
        </button>
      </div>
      <div className="modal fade" id={"news"+id} tabIndex="-1"
           aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-xl">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="modal-title"
                  id="exampleModalLabel">{news.title}</h2>
              <button type="button" className="btn-close"
                      data-bs-dismiss="modal" aria-label="Close">
              </button>
            </div>
            <div className="modal-body">
              <strong>Description : </strong>
              {news.description? news.description:'Click on source below to get the full article.'}
              <br />
              Published by <strong>{news.author? news.author:'anonymous author'}
              </strong> the {getDateStringWithSpaces(news.publishedAt)}
              <br />
              <strong>Source : </strong>
              <a href={news.url} title="Click to see full article">{news.source.name}</a>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default News;
